package tests;

import java.util.logging.Level;
import java.util.logging.Logger;
import matrices.DimensionesIncompatibles;
import matrices.Matriz;

public class Matrices {

    public static void main(String[] args) throws DimensionesIncompatibles {
        Matriz m1 = new Matriz(3, 3, true);
        System.out.println(m1);
        
        Matriz m2 = new Matriz(2, 3, true);
        System.out.println(m2);
        
        System.out.println(Matriz.Traspuesta(m1));
        System.out.println(Matriz.Adjunta(m1));
        System.out.println("Inversa"+Matriz.Inversa(m1));
        
        
        
        try {
            //System.out.println(Matriz.sumarDosMatrices(m1, m2));
            System.out.println(Matriz.Determinante(m1));
        } catch (DimensionesIncompatibles ex) {
            ex.printStackTrace();
        }
    }
    
}
